/* eslint-disable no-undef */

const jwt = require('jsonwebtoken')
const { Team } = require('../model/teamModel')

const auth = async (req, res, next) => {
  try {
    const token = req.headers["x-auth-token"];
    if (!token) return res.status(400).send({
      status: false,
      message: "A token is required for authentication"
    });
        // console.log("token", token);

    const decoded = await jwt.verify(token, 'hello_world')
    if (!decoded) return res.status(498).send({ status: false, message: "Invalid token" });

    // console.log("decoded", decoded);

    const user = await Team.findOne({ _id: decoded._id, 'tokens.token': token });

    // console.log("user>>>", user);
    // console.log("req.token>>>", req.token);
    // console.log("user>>>", user);

    if (!user) return res.send({ status: false, message: "invalid token" }) 

    req.token = token
    req.user = user
    next();
  } catch (error) {
    // console.log(error)
    res.status(401).send({ error: 'Please authenticate' })
  }
}

module.exports = auth
