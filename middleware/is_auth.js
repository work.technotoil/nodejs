/* eslint-disable no-undef */
// const { User } = require('../model/userModel')
const jwt = require('jsonwebtoken');
require('dotenv').config();

const checkItsAdminOrNot = async (req, res, next) => {
  const token = req.headers["x-access-token"];
  if (!token) {
    return res.status(400).send({
      status: false,
      message: "A token is required for authentication"
    });
  }
  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN);
    req.user = decoded;
    // console.log("decoded",decoded);
    // console.log(req.user.user.role);
    if (req.user.user.role === 'student') {
      return res.status(401).send({
        status: false,
        message: "You are not authorized to access",
      });
    }
  } catch (err) {
    return res.status(498).send({
      status: false,
      message: "Invalid token",
    });
  }
  return next();
}
module.exports = checkItsAdminOrNot;