/* eslint-disable no-dupe-keys */
/* eslint-disable no-redeclare */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
// const { User } = require('../model/teamModel');
// const mongoose = require('mongoose');

// const auth = require('../middleware/adminAuthenticate')

const { Team } = require('../model/teamModel');

const { TeamCheckInCheckOut } = require('../model/checkInCheckOutModel');

const { UserImage } = require('../model/imageModel');



// const moment = require('moment')
const multer = require('multer');
const bcrypt = require('bcrypt');
const { registrationValidate, emailValidate, id_validation} = require('../validation/joi.validate');
require('dotenv').config();

// =================================== SHOW LIST ALL TEAM DATA API ===================================
exports.getAllTeamData = async (req, res) => {
    const team = await Team.find().select('first_name last_name email technologies local_address primary_address contact_no gender fathers_name aadhar_no dateOfBirth role is_active')

    const dataCount = Team.count({}, function (err, result) {
        console.log(dataCount);
        // await Team.count({}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send({
                message: 'successfully',
                total: result,
                data: team
            });
        }
    });
}


// ================================= USER REPORT API ===========================================

exports.teamReportAPI = async (req, res) => {

    const data = await TeamCheckInCheckOut.aggregate([
        // {
        //     "$match": { "email": 'req.body.email' }
        // },
        // {

        //     "$project": {
        //         "date_diff": { "$subtract": ["$check_out_time", "$check_in_time"] }
        //     }
        // },
        // {
        //     "$project": {
        //         "days": { "$divide": ["$date_diff", 1000 * 60 * 60 * 24] },
        //         "hours": { "$divide": ["$date_diff", 1000 * 60 * 60] },
        //         "minutes": { "$divide": ["$date_diff", 1000 * 60] },
        //         "seconds": { "$divide": ["$date_diff", 1000] },
        //     }
        // },

        // important+++++
        // { $project:
        //     { duration:
        //       { $dateDiff: { startDate: "$check_in_time", endDate: "$check_out_time", unit: "minute" } }
        //     }
        //   }
        {
            // $match: { _id: new mongoose.Types.ObjectId('632c3387ab2c10bc051a4b5d') } dont remove this line
            $match: { email: req.body.email }
        },
        {
            $project:
            {
                //Email: "$email",
                Date: "$check_in_time",
                workingHours:
                    { $dateDiff: { startDate: "$check_in_time", endDate: "$check_out_time", unit: "hour" } },
                    
                Work: "$work_update"

            }
        }
    ])
    res.send({
        data: data
    })
    // console.log(">>", data);
}
//==============================user report by date ===================================


// ==================================== TEAM REGISTRATION API =====================================
exports.teamRegistrationPostApi = async (req, res) => {
    const { error } = registrationValidate(req.body);
    if (error) return res.status(400).json({ status: false, message: error.message });

    try {
        const findTeamMember = await Team.findOne({ email: req.body.email });
        if (findTeamMember) return res.status(400).send({
            status: false,
            message: 'Team member already registered.'
        });

        var saveTeam = new Team({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            password: req.body.password,
            primary_address: req.body.primary_address,
            local_address: req.body.local_address,
            contact_no: req.body.contact_no,
            technologies: req.body.technologies,
            gender: req.body.gender,
            fathers_name: req.body.fathers_name,
            fathers_contact_no: req.body.fathers_contact_no,
            date_of_birth: req.body.date_of_birth,
            aadhar_no: req.body.aadhar_no,
            account_no: req.body.account_no,
            role: req.body.role,
            is_active: req.body.is_active,
        });
        console.log("date",saveTeam.date_of_birth);
        const salt = await bcrypt.genSalt(10);
        saveTeam.password = await bcrypt.hash(saveTeam.password, salt);

        const token = await saveTeam.newAuthToken();

        saveTeam = await saveTeam.save();
        res.status(201).json({
            status: true,
            message: "Register successfully",
            name: saveTeam.first_name,
            token: token
        });
          console.log(saveTeam);
    } catch (error) {
        console.log("Got some error", error);
    }
};


// =======================================TEAM MEMBER EDIT API========================================

exports.teamEditApi = async (req, res) => {
    const { error } = id_validation(req.params);
    if (error) {
        return res.status(400).json({
            status: false,
            message: error.message
        });
    }
    try {
        const updateTeamMember = await Team.findByIdAndUpdate(req.params.id,
            {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: req.body.password,
                local_address: req.body.local_address,
                contact_no: req.body.contact_no,
                technologies: req.body.technologies,
                gender: req.body.gender,
                fathers_name: req.body.fathers_name,
                fathers_contact_no: req.body.fathers_contact_no,
                date_of_birth: req.body.date_of_birth,
                aadhar_no: req.body.aadhar_no,
                account_no: req.body.account_no,
                role: req.body.role,
                is_active: req.body.is_active,
            }, { new: true });

        if (!updateTeamMember) return res.status(404).send({
            status: false,
            message: 'The team member with the given ID was not found'
        });
        res.status(200).send({
            status: true,
            message: "Updated successfully",
            // updateTeamMember
        });
    } catch (error) {
        console.log(error);
    }
}
// ======================================TEAM MEMBER DETAILS API=======================================

exports.teamDetailsApi = async (req, res) => {
    const { error } = emailValidate(req.body);
    if (error) {
        return res.status(400).json({
            status: false,
            message: error.message
        });
    }
    const email = req.body.email;
    const users = [];

    try {
        const updateTeamMember = await TeamCheckInCheckOut.find({ email: email });
        if (!updateTeamMember) return res.status(404).send({
            status: false,
            message: 'The team member with the given ID was not found'
        });

        const count_data = await TeamCheckInCheckOut.countDocuments({ email: email });

        // console.log("jolo", updateTeamMember);

        updateTeamMember.map((item) => {

            users.push({
                "check_in_time": item.check_in_time,
                "check_out_time": item.check_out_time,
                "work_update": item.work_update
            })
        })
        console.log("hello", users);
        res.status(200).send({
            status: true,
            // message: "Updated successfully",
            total: count_data,
            users,
        });
    } catch (error) {
        console.log(error);
    }
}

// ============================== TEAM MEMBER DELETE API ===============================
exports.teamDeleteApi = async (req, res) => {

    const deleteTeamMember = await Team.findByIdAndRemove(req.body.id);

    if (!deleteTeamMember) {
        return res.status(404).send({
            status: false,
            id: req.params.id,
            message: 'The Team Member with the given ID was not found'
        })
    } else {
        res.send({
            success: true,
            name: deleteTeamMember.first_name,
            message: 'Deleted successfully'
        });
    }
}


// ========================= SHOW LIST ALL TEAM MEMBERS FOR PROJECT API ===========================
exports.listAllTeamMember = async (req, res) => {
    const memberList = await Team.find().select('-_id first_name last_name')

    const dataCount = Team.count({}, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.send({
                status: true,
                total: result,
                data: memberList
            });
        }
    });
}



//================== USER PROFILE IMAGE UPLOAD API =======================

const Storage = multer.diskStorage({
    destination: 'uploads',
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    },
});

const upload = multer({ storage: Storage }).single('testImage')

exports.userimageUpload = async (req, res) => {
    upload(req, res, (err) => {
        if (err) {
            console.log(err)
        }
        else {
            const newImage = new UserImage({
                name: req.body.name,
                image: {
                    data: req.file.filename,
                    contentType: 'image/png'
                }
            })
            newImage.save()
                .then(() => res.send('successfully uploaded'))
                .catch(err => err.message === 'Error uploading image')
        }
    })
}
