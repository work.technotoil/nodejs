/* eslint-disable no-dupe-keys */
/* eslint-disable no-redeclare */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const { Projects } = require('../model/projectsModel');

const {project_validation} = require('../validation/joi.validate');
require('dotenv').config();

//======================== PROJECT ADD API  ==========================================

exports.projectAddApi = async (req, res) => {

    const { error } = project_validation(req.body);
    if (error) return res.status(400).json({ status: false, message: error.message });

    const Project_name = req.body.Project_name;

    const ProjectCheck = await Projects.findOne({ Project_name: Project_name })

    if (ProjectCheck == null) {

        var project = new Projects({
            Project_name: Project_name,
            Members: req.body.Members,
            Technology: req.body.Technology,
            Leader: req.body.Leader,
            is_active: req.body.is_active,
            Start_date: req.body.Start_date,
            Client_name: req.body.Client_name,
            Client_address: req.body.Client_address,
        })

        projectData = await project.save();
        res.status(201).json({
            status: true,
            message: 'Project added successfully'
        });
    } else {
        res.send({
            status: false,
            message: 'Project already registered'
        })
    }
}

//============================ PROJECT  LIST  API =====================================

exports.projectListApi = async (req, res) => {
    const listOfProject = await Projects.find({}).select('-__v -_id');

    count_project = await Projects.countDocuments({});

    res.send({
        success: true,
        count_project,
        listOfProject
    })
};


//============================ PROJECT EDIT API ========================================

exports.projectEditApi = async (req, res) => {

    try {

        const project_id_check_db = await Projects.findOne({ '_id': req.params.id })

        if (!project_id_check_db) return res.status(404).send({
            status: false,
            message: 'Project not found'
        });
        const updateProject = await Projects.findByIdAndUpdate(req.params.id,
            {
                Project_name: req.body.Project_name,
                Members: req.body.Members,
                Technology: req.body.Technology,
                Leader: req.body.Leader,
                is_active: req.body.is_active,
                Start_date: req.body.Start_date,
                Client_name: req.body.Client_name,
                Client_address: req.body.Client_address,
            }, { new: true });

        res.status(200).send({
            status: true,
            message: "Update successfully",
            updateProject
        });
    } catch (error) {
        console.log(error);
    }
};