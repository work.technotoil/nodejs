/* eslint-disable no-dupe-keys */
/* eslint-disable no-redeclare */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const { User } = require('../model/teamModel');

// const { Team } = require('../model/teamModel');
const { Tech } = require('../model/techModel');
// const { TeamCheckInCheckOut } = require('../model/checkInCheckOutModel');

// const { ExpensesTitle } = require('../model/expensesListModel');
// const { Expenses } = require('../model/expensesModel');
// const { Projects } = require('../model/projectsModel');

// const moment = require('moment')
// const bcrypt = require('bcrypt');
const { tech_validation} = require('../validation/joi.validate');
require('dotenv').config();




// ======================================= SHOW ALL TECHNOLOGY LIST API ================================
exports.listAllTechnology = async (req, res) => {
    const listOfTech = await Tech.find({ is_active: true }).select('_id is_active technology');
    // const listOfTech = await Tech.findAll() 
console.log(listOfTech);
    // const technologyCount = Tech.countDocuments({});
    // .countDocuments({ email: email });
    // const dataCount = Team.count({}
    res.send({
        status: true,
        message: 'success',
        // dd: technologyCount,
        data: listOfTech,
    });
}
// let techno = [];
// const team = await Tech.find();
// team.map(async (ele) => {
//     if (ele.is_active === true) {
//         techno.push({
//             "id": ele.id,
//             "technology": ele.technology,
//         })
//         const team_modi = await Tech.find().select('id technology');
//     }
// })
// ======================================= EDIT TECHNOLOGY API ================================
exports.editTechnology = async (req, res) => {
    try {
        const updateTechnology = await Tech.findByIdAndUpdate(req.body.id,
            {
                technology: req.body.technology,
                is_active: req.body.is_active,
            }, { new: true });

        if (!updateTechnology) return res.status(404).send({
            status: false,
            message: 'Technology not found'
        });
        res.status(200).send({
            status: true,
            message: "Update successfully",
            updateTechnology
        });
    } catch (error) {
        console.log(error);
    }
}




// =================================== TEAM ADD TECHNOLOGY API =======================================
exports.addTechnology = async (req, res) => {
    const { error } = tech_validation(req.body);
    if (error) {
        return res.status(400).json({
            status: false,
            message: error.message
        });
    }
    try {
        const add_technology = req.body.technology;
        const is_active = req.body.is_active;
        const checkDb = await Tech.findOne({
            technology: add_technology,
            // technology: { $regex: '.*' + req.body.technology + '.*' }
            technology: { $regex: '.*' + req.body.technology + '.*', $options: 'i' }
        }).select("technology");

        if (checkDb) return res.status(400).send({
            status: false,
            message: 'This technology already added'
        });
        var saveTech = new Tech({
            technology: req.body.technology,
            is_active: req.body.is_active
        });
        // console.log("bello",saveTech);
        saveTech = await saveTech.save();
        res.status(201).json({
            status: true,
            message: "Register successfully",
        });
    } catch (error) {
        console.log(error);
    }
}