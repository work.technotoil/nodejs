/* eslint-disable no-dupe-keys */
/* eslint-disable no-redeclare */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const { ExpensesTitle } = require('../model/expensesListModel');
const { Expenses } = require('../model/expensesModel');
//const { TeamExpenses } = require('../model/TeamExpensesModel')


const { expenses_validation, TeamExpenses_validation } = require('../validation/joi.validate');
require('dotenv').config();

//============================ Expenses Title ADD API ========================================

exports.expensesTitleAdd = async (req, res) => {
    
    try {
        const title = req.body.title;
        console.log(title);

        const ExpensesTitleCheck = await ExpensesTitle.findOne({ title: title })

        if (ExpensesTitleCheck == null) {
            var expensesTitle = new ExpensesTitle({
                title: title,
            })

            expensesTitleData = await expensesTitle.save();
            res.status(201).json({
                status: true,
                message: 'Expenses title added successfully'
            });
        } else {
            res.send({
                status: false,
                message: 'Expenses title already registered'
            })
        }
    } catch (error) {
        console.log(error);
    }
}
//============================ Expenses Title List API ========================================

exports.expensesTitleList = async (req, res) => {

    try {
        const expensesTitleList = await ExpensesTitle.find({}).select('-__v -_id -createdAt -updatedAt ');

        // count_project = await Projects.countDocuments({});
        res.send({
            success: true,
            // count_project,
            listOfExpensesTitle: expensesTitleList
        })
    } catch (error) {
        console.log(error);
    }
}

//============================ Expenses ADD API ========================================


exports.expensesAdd = async (req, res) => {

    const { error } = expenses_validation(req.body);
    if (error) return res.status(400).json({ status: false, message: error.message });

    const Title = req.body.Title;
    const Amount = req.body.Amount;
    const Expense_details = req.body.Expense_details;
    const Name = req.body.Name;


    //const ExpensesCheck = await Expenses.findOne({ Title: Title })

    // if (ExpensesCheck == null) {
    var expenses = new Expenses({
        Title: Title,
        Amount: Amount,
        Expense_details: Expense_details,
        Name: Name
    })

    expenses = await expenses.save();
    // res.status(201).json({
    //     status: true,
    //     message: 'expenses added successfully'
    // });
    // } else {
    res.send({
        status: true,
        message: 'Expenses added successfully'
    })
}



//============================ Expenses  LIST  API ========================================  


exports.expensesList = async (req, res) => {
    try {
        const expensesList = await Expenses.find({}).select('-__v -_id');

        count_expenses = await Expenses.countDocuments({});

        res.send({
            success: true,
            count_expenses,
            listOfExpenses: expensesList
        })
    } catch (error) {
        console.log(error);
    }
}


//============================ Expenses EDIT API ========================================  

exports.expensesEdit = async (req, res) => {

    try {
        const expenses_id_check_db = await Expenses.findOne({ '_id': req.params.id })

        if (!expenses_id_check_db) return res.status(404).send({
            status: false,
            message: 'Expenses not found'
        });
        const updateExpenses = await Expenses.findByIdAndUpdate(req.params.id,
            {
                Title: req.body.Title,
                Amount: req.body.Amount,
                Expense_details: req.body.Expense_details,
                Name:req.body.Name

            }, { new: true });

        res.status(200).send({
            status: true,
            message: "Update successfully",
            updateExpenses
        });
    } catch (error) {
        console.log(error);
    }
}

//======================= STATUS UPDATE API IN EXPENSE ===================================
exports.statusUpdateinexpense = async (req, res) => {

    
        const updateExpenses = await Expenses.findByIdAndUpdate(req.params.id,
            {
                Status : req.body.Status

            }, { new: true });

        res.status(200).send({
            status: true,
            message: "Update successfully",
            updateExpenses
        });
    } 
//============================ Expenses DELETE API ======================================== 


exports.expenseDelete = async (req, res) => {

    const deleteExpenses = await Expenses.findByIdAndRemove(req.body.id);

    if (!deleteExpenses) {
        return res.status(404).send({
            status: false,
            id: req.params.id,
            message: 'Expenses with the given ID was not found'
        })
    } else {
        res.send({
            success: true,
            name: deleteExpenses.first_name,
            message: 'Deleted successfully'
        });
    }
}



//============================ TEAM Expenses ADD API ======================================== 

// exports.TeamExpensesAdd = async (req, res) => {

//     const { error } = TeamExpenses_validation(req.body);
//     if (error) return res.status(400).json({ status: false, message: error.message });

//     const Team_Member_Name = req.body.Team_Member_Name;
//     const Amount = req.body.Amount;
//     const Expense_name = req.body.Expense_name

//     var teamExpenses = new TeamExpenses({
//         Team_Member_Name: Team_Member_Name,
//         Amount: Amount,
//         Expense_name : Expense_name

//     });

//     teamExpenses = await teamExpenses.save();
//     res.send({
//         status: true,
//         message: 'Team Expenses added successfully'
//     })
// }

//============================ TEAM Expenses LIST API ======================================== 

// exports.TeamExpensesList = async (req, res) => {
//     try {
//         const TeamExpensesList = await TeamExpenses.find({}).select('-__v -_id');

//         count_Teamexpenses = await TeamExpenses.countDocuments({});

//         res.send({
//             success: true,
//             count_Teamexpenses,
//             listOfExpenses: TeamExpensesList
//         })
//     } catch (error) {
//         console.log(error);
//     }
// }

//============================ TEAM Expenses EDIT API ======================================== 

// exports.TeamExpensesEdit = async (req, res) => {
//     const { error } = TeamExpenses_validation(req.body);
//     if (error) return res.status(400).json({ status: false, message: error.message });

//     try {
//         const teamExpenses_id_check_db = await TeamExpenses.findOne({ '_id': req.params.id })

//         if (!teamExpenses_id_check_db) return res.status(404).send({
//             status: false,
//             message: 'Team Expenses not found'
//         });
//         const updateTeamExpenses = await TeamExpenses.findByIdAndUpdate(req.params.id,
//             {
//                 Team_Member_Name: req.body.Team_Member_Name,
//                 Amount: req.body.Amount

//             }, { new: true });

//         res.status(200).send({
//             status: true,
//             message: "Update successfully",
//             updateTeamExpenses
//         });
//     } catch (error) {
//         console.log(error);
//     }
// }

//============================ TEAM Expenses DELETE API ======================================== 

// exports.TeamexpenseDelete = async (req, res) => {

//     const deleteTeamExpenses = await TeamExpenses.findByIdAndRemove(req.body.id);

//     if (!deleteTeamExpenses) {
//         return res.status(404).send({
//             status: false,
//             id: req.params.id,
//             message: 'Team Expenses with the given ID was not found'
//         })
//     } else {
//         res.send({
//             success: true,
//             name: deleteTeamExpenses.first_name,
//             message: 'Deleted successfully'
//         });
//     }
// }
//============================ OFFICE EXPENSES REPORT API ======================================== 
 
exports.officeExpensesReportAPI = async (req, res) => {
    const data = await Expenses.aggregate([
        {
            // $match: { _id: new mongoose.Types.ObjectId('632c3387ab2c10bc051a4b5d') } dont remove this line
            $match: { Title: req.body.Title }
        },
        {
            $project:
            {
                //Email: "$email",
                //Date: "$check_in_time",
                TotalAmount:
                 {$sum: [  "$Amount" ] },
              //  TotalAmount:
              // {$group : {_id:"$Team_Member_Name", count:{$sum:"$Amount"}}}
             }
        },
    ])
    let total=0
    for (var i  = 0; i < data.length; i++){
        if(data[i].TotalAmount){
            total  += data[i].TotalAmount;
        }
    }
    res.send({
        data: data,
        OverallAmount: total
        // Main: data
        
    })

}


//============================ TEAM EXPENSES REPORT API ======================================== 

exports.TeamexpensesReportAPI = async (req, res) => {

    const data = await Expenses.aggregate([

        {
            // $match: { _id: new mongoose.Types.ObjectId('632c3387ab2c10bc051a4b5d') } dont remove this line
            $match: { Name: req.body.Name }
        },
        {
            $project:
            {
                //Email: "$email",
                //Date: "$check_in_time",
                TotalAmount:
                    {$sum: [  "$Amount" ] },
                //TotalAmount:
               //{$group : {_id:"$Team_Member_Name", count:{$sum:"$Amount"}}}
             }
        },
    ])

    //console.log("data" ,data);
    let total=0
    for (var i  = 0; i < data.length; i++){
        if(data[i].TotalAmount){
            total  += data[i].TotalAmount;
        }
    }
    //console.log("total" , total);
    res.send({
        data: data,
        OverallAmount: total
        // Main: data
        
    })

}
