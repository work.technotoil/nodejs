/* eslint-disable no-inner-declarations */
/* eslint-disable no-unused-vars */
/* eslint-disable no-redeclare */
/* eslint-disable no-undef */
const Joi = require('joi');
const bcrypt = require('bcrypt');
// const auth = require('../middleware/auth');
require('dotenv').config();

const { Team } = require('../model/teamModel');
const { TeamCheckInCheckOut } = require('../model/checkInCheckOutModel');
const { loginValidate, emailValidate } = require('../validation/joi.validate');

// =======================================TEAM LOGIN API========================================
exports.teamLoginPostApi = async (req, res) => {
    const { error } = loginValidate(req.body);
    if (error) { return res.status(400).json({ status: false, message: error.message }) }

    const email = req.body.email;
    const password = req.body.password;
    console.log("bello", password);

    await Team.findOne({ email: email })
        .then(async team => {
            if (!team) {
                res.json({
                    status: false,
                    message: 'You are new user please sign up first'
                })
            }
            bcrypt
                .compare(password, team.password)
                .then(async doMatch => {

                    const accessToken = await team.newAuthToken()
                    if (doMatch) {
                        res.json({
                            status: true,
                            name: team.first_name + team.last_name,
                            message: 'Login successfully',
                            token: accessToken
                        });
                        await team.save()
                    } else {
                        res.send({
                            status: false,
                            message: 'Invalid credential'
                        })
                    }
                })
                .catch(err => {
                    console.log(err);
                    res.json({
                        status: true,
                        message: 'Got some error'
                    });
                });
        })
        .catch(err => console.log(err));
}

// =============================== TEAM CHECK-IN API =================================
exports.teamCheckInApi = async (req, res) => {

    try {
        let dbDate
        let get_date_from_checkInDb
        const { error } = emailValidate(req.body);
        if (error) { return res.status(400).json({ status: false, message: error.message }) }

        const check_in_date = new Date();
        const boot = `${check_in_date.getFullYear()}/${check_in_date.getMonth() + 1}/${check_in_date.getDate()}`;

        const check_In_Db = await Team.findOne({ email: req.body.email }); // checking email from team database 
        if (!check_In_Db) return res.status(400).send({ status: false, message: 'User not found' });

        const checkIN_time_check = await TeamCheckInCheckOut.findOne({ email: req.body.email }).sort({ _id: -1 }).limit(1)

        if (checkIN_time_check == null) {
            console.log("im inside if");
            const check_in = new TeamCheckInCheckOut({
                email: req.body.email,
                check_in_time: check_in_date,
            });
            await check_in.save();

            return res.json({
                status: true,
                message: "Check IN successfully",
                time: check_in_date,
                _id: check_in._id,
            });
        }

        get_date_from_checkInDb = checkIN_time_check.check_in_time;

        dbDate = `${get_date_from_checkInDb.getFullYear()}/${get_date_from_checkInDb.getMonth() + 1}/${get_date_from_checkInDb.getDate()}`

        if (dbDate === boot) {
            console.log("im inside 2nd if");
            res.status(400).send({
                status: false,
                message: 'You are already checked IN'
            })
        } else {
            console.log("im inside else");
            const check_in = new TeamCheckInCheckOut({
                email: req.body.email,
                check_in_time: check_in_date,
            });
            await check_in.save();
            res.status(201).end({
                status: true,
                message: "Check IN successfully",
                time: check_in_date,
                "id": check_in.id,
            });
        }
    } catch (error) {
        res.send({
            status: false,
            message: `got some error ${error}`
        })
    }
}

// ================================= TEAM CHECK-OUT API ====================================

exports.checkOutAPI = async (req, res) => {
    try {
        const work_update = req.body.work_update;

        const checkOutDate = new Date();

        const checkIN_time_check = await TeamCheckInCheckOut.findOne({ "_id": req.params.id });

        if (!checkIN_time_check) return res.status(400).send({ status: false, message: 'User with given ID not found' })

        const db_date = checkIN_time_check.check_out_time;

        if (db_date == undefined) {
            const updateTeamMember = await TeamCheckInCheckOut.findByIdAndUpdate(req.params.id, {
                check_out_time: checkOutDate,
                work_update: work_update
            }, { new: true });

            if (!updateTeamMember) return res.status(404).send({
                status: false,
                message: 'Update false'
            });
            res.status(200).send({
                status: true,
                message: "Check OUT successfully"
            });
        } else {
            res.status(400).send({
                status: false,
                message: 'You are already checked out'
            })
        }
    } catch (error) {
        console.log(error);
    }
}

// ================================ TEAM LOGOUT POST API ==================================
exports.logoutPostApi = async (req, res) => {

    try {
        req.user.tokens = await req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save();
        res.status(200).send({
            status: true,
            message: 'Logout successfully'
        })
    } catch (error) {
        res.status(500).send({
            status: false,
            message: `got some error${error}`
        });
    }
}





