/* eslint-disable no-undef */
const mongoose = require('mongoose');


const CheckInCheckOutSchema = new mongoose.Schema({
    // user: {
    //     type: mongoose.Schema.ObjectId,
    //     ref: 'TeamMember'
    // },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        // unique: true,
        // required: true
    },
    check_in_time: {
        type: Date,
        // required: true
    },
    check_out_time: {
        type: Date,
        // required: true
    },
    work_update: {
        type: String,
        // required: true
    },
    
    // created_at: {
    //     type: Date,
    //     required: true,
    //     default: Date.now
    // },
    // updated_at: {
    //     type: Date,
    //     required: true,
    //     default: Date.now
    // }
},
// {
//     timestamps: true
// }
);

const TeamCheckInCheckOut = mongoose.model('Check_in_out', CheckInCheckOutSchema);

// exports.TeamCheckInCheckOut = TeamCheckInCheckOut;

exports.TeamCheckInCheckOut = TeamCheckInCheckOut