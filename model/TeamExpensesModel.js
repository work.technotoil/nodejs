/* eslint-disable no-undef */
const mongoose = require('mongoose');


const TeamExpensesSchema = new mongoose.Schema({
    Team_Member_Name: {
        type: String
    },
    Amount : {
        type: Number
    },
    Expense_name : {
        type : String
    }
    
},
    {
        timestamps: true
    }
);

const TeamExpenses = mongoose.model('TeamExpenses', TeamExpensesSchema);

exports.TeamExpenses = TeamExpenses