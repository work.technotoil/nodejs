/* eslint-disable no-undef */
const mongoose = require('mongoose');


const ExpensesSchema = new mongoose.Schema({
    Title: {
         type: String
        // type:  [{ type: mongoose.Types.ObjectId, ref: 'ExpensesTitleList' }]
    },
    Amount : {
        type: Number
    },
    Expense_details:{
        type: String
    },
    Name : {
        type: String
    },
    Status : {
        type: Boolean,
        default : false
    }

},
    {
        timestamps: true
    }
);

const Expenses = mongoose.model('ExpensesList', ExpensesSchema);

exports.Expenses = Expenses