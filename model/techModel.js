/* eslint-disable no-undef */
const mongoose = require('mongoose');

const Tech = mongoose.model('Technology', new mongoose.Schema({
    
    technology: {
        type: String,
        trim: true,
        required: true
    },
    is_active: {
        type: Boolean,
        required: true,
        default: true
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now
    }
}));


exports.Tech = Tech;