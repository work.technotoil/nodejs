/* eslint-disable no-undef */
// eslint-disable-next-line no-undef
const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    image : { 
        // eslint-disable-next-line no-undef
        data:Buffer,
        contenttype:String
    }
})


const UserImage = mongoose.model('Image', ImageSchema);
exports.UserImage = UserImage