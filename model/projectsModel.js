/* eslint-disable no-undef */
// eslint-disable-next-line no-undef
const mongoose = require('mongoose');


const ProjectSchema = new mongoose.Schema({

    Project_name: {
        type: String,
       // unique: true,
        // required: true
    },
    Members:{
        type: String,
       // unique: true,
        // required: true
    },
    is_active:{
        type: Boolean,
       // unique: true,
        // required: true
    },
    Technology:{
        type: String,
       // unique: true,
       // required: true
    },
    Leader:{
        type: String,
       // unique: true,
       // required: true
    },
    
    Start_date:{
        type: String,
        // required: true
    },
    Client_name:{
        type: String,
       // unique: true,
      // required: true
    },
    Client_address:{
        type: String,
       // unique: true,
      // required: true
    }
},
)

const Projects = mongoose.model('Projects', ProjectSchema);

exports.Projects = Projects

