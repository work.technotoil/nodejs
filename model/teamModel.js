/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
// const { ObjectID } = require('bson');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

// const Team = mongoose.model('TeamMember', new mongoose.Schema({

const TeamSchema = new mongoose.Schema({
    first_name: {
        type: String,
        minlength: 3,
        maxlength: 50,
        default: false
    },
    last_name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
        default: false
    },
    primary_address: {
        type: String,
        required: true
    },
    local_address: {
        type: String,
        required: true,
    },
    contact_no: {
        type: Number,
        required: true
    },
    
    technologies: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    fathers_name: {
        type: String,
        required: true
    },
    fathers_contact_no: {
        type: Number,
        required: true
    },
    date_of_birth: {
        type: Date,
        required: true,
         //default: Date.now
    },
    aadhar_no: {
        type: Number,
        required: true
    },
    account_no: {
        type: Number,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    is_active: {
        type: Boolean,
        required: true
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now
    }
});
TeamSchema.methods.newAuthToken = async function () {
    const user = this
    // const token = jwt.sign({ _id: user.id.toString() }, 'hello_world', { expiresIn: "2m" });
    const token = jwt.sign({ _id: user.id.toString() }, 'hello_world', { expiresIn: "1y" });
    // const token = jwt.sign({ user }, 'hello_world', { expiresIn: "7 days" });

    // console.log("bhopal>>>>>", this);
    // console.log("token>>>>>", token);
    // console.log(">>>>>userToken", user.tokens);   

    user.tokens = user.tokens.concat({ token });
    await user.save();
    return token;
}

const Team = mongoose.model('TeamMember', TeamSchema);

exports.Team = Team;


