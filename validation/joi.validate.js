/* eslint-disable no-undef */
const Joi = require('joi');

// ===================Sign Up validation =====================

function registrationValidate(register) {
    const schema = Joi.object({
        first_name: Joi.string().required().error(err => { err[0].message = "first_name is required"; return err; }),
        last_name: Joi.string().required().error(err => { err[0].message = "last_name is required"; return err; }),
        email: Joi.string().email().required().error(err => { err[0].message = "Email is required"; return err; }),
        password: Joi.string().required().error(err => { err[0].message = "Password is required"; return err; }),
        primary_address: Joi.string().required().error(err => { err[0].message = "primary_address is required"; return err; }),
        local_address: Joi.string().required().error(err => { err[0].message = "local_address is required"; return err; }),
        technologies: Joi.string().required().error(err => { err[0].message = "technology is required"; return err; }),
        contact_no: Joi.number().required().error(err => { err[0].message = "contact_no is required"; return err; }),
        gender: Joi.string().required().error(err => { err[0].message = "gender is required"; return err; }),
        fathers_name: Joi.string().required().error(err => { err[0].message = "father_name is required"; return err; }),
        fathers_contact_no: Joi.number().required().error(err => { err[0].message = "father_mobile is required"; return err; }),
        role: Joi.string().required().error(err => { err[0].message = "role is required"; return err; }),
        // dob: Joi.date().format(['YYYY/MM/DD', 'DD-MM-YYYY']).required().error(err => { err[0].message = "dob is required"; return err; }),
        // dateOfBirth: Joi.date().raw().required().required().error(err => { err[0].message = "dateOfBirth is required"; return err; }),
        date_of_birth: Joi.date().raw().required().error(err => { err[0].message = "date_of_birth is required"; return err; }),
        aadhar_no: Joi.number().required().error(err => { err[0].message = "aadhar_no is required"; return err; }),
        account_no: Joi.number().required().error(err => { err[0].message = "account_no is required"; return err; }),
        is_active: Joi.boolean().required().error(err => { err[0].message = "is_active is required"; return err; }),
    });
    return schema.validate(register);
}

// ==================== Login validation =====================
function loginValidate(teamLogin) {
    const schema = Joi.object({
        email: Joi.string().email().required().error(err => { err[0].message = "Email is required"; return err; }),
        password: Joi.string().required().error(err => { err[0].message = "Password is required"; return err; }),
    });
    return schema.validate(teamLogin);
}
// ================== validation for email ===================
function emailValidate(checkIn) {
    const schema = Joi.object({
        email: Joi.string().email().required().error(err => { err[0].message = "email is required"; return err; }),
    });
    return schema.validate(checkIn);
}
// ================== validation for email ===================
function id_validation(feed) {
    const schema = Joi.object({
        id: Joi.string().required().error(err => { err[0].message = "ID is required"; return err; }),
    });
    return schema.validate(feed);
}
// ================== validation for technology ===================
function tech_validation(tech) {
    const schema = Joi.object({
        // technology: Joi.string().trim().required().error(err => { err[0].message = "Technology is required"; return err; }),
        technology: Joi.string().trim().required().error(err => { err[0].message = "Technology is required"; return err; }),
        is_active: Joi.boolean().required().error(err => { err[0].message = "is_active is required"; return err; }),
    });
    return schema.validate(tech);
}
// ================== validation for project =======================
// eslint-disable-next-line no-unused-vars
function project_validation(project) {
    const schema = Joi.object({
        Project_name: Joi.string().alphanum().required().error(err => { err[0].message = "Project_name is required"; return err; }),
        Members: Joi.string().required().error(err => { err[0].message = "Members is required"; return err; }),
        Technology: Joi.string().required().error(err => { err[0].message = "Technology is required"; return err; }),
        Leader: Joi.string().required().error(err => { err[0].message = "Leader is required"; return err; }),
        Start_date: Joi.string().required().error(err => { err[0].message = "Start_date is required"; return err; }),
        is_active: Joi.boolean().required().error(err => { err[0].message = "is_active is required"; return err; }),
        Client_name: Joi.string().required().error(err => { err[0].message = "Client_name is required"; return err; }),
        Client_address: Joi.string().required().error(err => { err[0].message = "Client_address is required"; return err; }),
        });
    return schema.validate(project);

}

// ================== validation for expenses =======================

function expenses_validation(expenses) {

    const schema = Joi.object({
        Title: Joi.string().required().error(err => { err[0].message = "Title is required"; return err; }),
        Amount: Joi.number().integer().min(1).required().error(err => { err[0].message = "Amount is required"; return err; }),
        Expense_details: Joi.string().required().error(err => { err[0].message = "Expense_details is required"; return err; }),
        Name: Joi.string().required().error(err => { err[0].message = "Name is required"; return err; }),
    });
    return schema.validate(expenses);

}

// ================== validation for team expenses =======================

function TeamExpenses_validation(TeamExpenses) {

    const schema = Joi.object({
        Team_Member_Name: Joi.string().required().error(err => { err[0].message = "Team Member Name is required"; return err; }),
        Amount:Joi.number().integer().min(1).required().error(err => { err[0].message = "Amount is required"; return err; }),
        Expense_name:Joi.string().required().error(err => { err[0].message = "Expense_name is required"; return err; }),
        });
    return schema.validate(TeamExpenses);
    } 

module.exports = { registrationValidate, loginValidate,  emailValidate, id_validation, tech_validation, project_validation, expenses_validation,TeamExpenses_validation }