/* eslint-disable no-undef */
const express = require('express')
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const http = require('http')
var cors = require('cors')
var app = express();
const PORT = 8000


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(express.json());
 app.use(cors({
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
 }));


const teamRouter = require('./routes/teamRouter')
const adminRouter = require('./routes/adminRouter');
const projectRouter = require('./routes/projectRouter');
const technologyRouter = require('./routes/technologyRouter');
const expensesRouter = require('./routes/expensesRouter');


// mongoose.connect('mongodb+srv://parul:Q9uNp6YY515D4Ubq@cluster0.kwanmtq.mongodb.net/?retryWrites=true&w=majority')

// mongoose.connect('mongodb://localhost/technoToil', {
// mongoose.connect('mongodb://localhost/technotoil_project', {
// mongoose.connect('mongodb+srv:cc@cluster0.ty60ki2.mongodb.net/?retryWrites=true&w=majority', {
 mongoose.connect('mongodb+srv://sexybaba:nodejs12345@cluster0.ty60ki2.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true
}).then(() => {
    console.log('connected to database');
}).catch((err) => {
    console.log('failed connected to database', err);
});


app.use(teamRouter);

app.use(adminRouter);

app.use(projectRouter);

app.use(technologyRouter);

app.use(expensesRouter);


http.createServer(app).listen(PORT, () => console.log(`Listening on port ${PORT}...`));
