/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const router = require('express').Router();
const adminAuth = require('../middleware/adminAuthenticate');

const expensesController = require('../controllers/expensesController');


// =============== Expenses ===================
// router.post('/expenses/list/add', adminController.expensesAdd);
// =============== Expenses title ===================
//router.post('/expensestitle/add', expensesController.expensesTitleAdd);
router.post('/expensestitle/add', expensesController.expensesTitleAdd);

router.get('/expensestitle/list', expensesController.expensesTitleList);


// =============== Expenses  ===================
router.post('/expenses/add', expensesController.expensesAdd);

router.get('/expenses/list', expensesController.expensesList);

router.post('/expenses/edit/:id', expensesController.expensesEdit);

router.delete('/expenses/delete', expensesController.expenseDelete);

router.post('/expenses/status/edit/:id', expensesController.statusUpdateinexpense);

// =============== Team Expenses  ===================

// router.post('/teamexpenses/add', expensesController.TeamExpensesAdd);

// router.get('/teamexpenses/list', expensesController.TeamExpensesList);

// router.post('/teamexpenses/edit/:id', expensesController.TeamExpensesEdit);

// router.delete('/teamexpenses/delete', expensesController.TeamexpenseDelete);

 router.post('/teamexpeneses/report' , expensesController.TeamexpensesReportAPI );

router.post('/officeexpeneses/report' , expensesController.officeExpensesReportAPI );

// router.post('/expensesdatareportbydate/report' , expensesController.expensebetweenDate)



module.exports = router;
