/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const router = require('express').Router();
const adminAuth = require('../middleware/adminAuthenticate');

const technologyController = require('../controllers/technologyController');

// =============== Technology =================
router.post('/add/technology', technologyController.addTechnology);

router.get('/list/technology', technologyController.listAllTechnology);

router.put('/edit/technology', technologyController.editTechnology);


module.exports = router;