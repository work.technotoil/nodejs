/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const router = require('express').Router();
const teamController = require('../controllers/teamController');

const authenticate = require('../middleware/adminAuthenticate');

router.post('/team/login',  teamController.teamLoginPostApi);

router.post('/team/check_in',  teamController.teamCheckInApi);

// router.post('/team/check_out/:id',  teamController.teamCheckOutApi);
// router.post('/team/check_out',  teamController.teamCheckOutApi);

router.post('/team/check_out/:id',  teamController.checkOutAPI);

router.post('/team/logout', authenticate,  teamController.logoutPostApi);

module.exports = router;