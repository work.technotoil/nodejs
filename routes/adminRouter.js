/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const router = require('express').Router();
const adminAuth = require('../middleware/adminAuthenticate');

const adminController = require('../controllers/adminController');


// router.post('/team/registration', adminController.teamRegistrationPostApi);
router.post('/team/registration', adminController.teamRegistrationPostApi);

router.post('/list', adminController.getAllTeamData);

router.get('/list/member', adminController.listAllTeamMember);

router.post('/team/details', adminController.teamDetailsApi);

router.post('/team/edit/:id',  adminController.teamEditApi);

// router.post('/team/edit', adminController.teamEditpi);

// router.post('/team/delete/:id', adminController.teamDeleteApi);
//router.post('/team/delete', adminAuth, adminController.teamDeleteApi);
router.post('/team/delete', adminController.teamDeleteApi);

router.post('/team/report', adminController.teamReportAPI);



router.post('/team/images/upload', adminController.userimageUpload);


module.exports = router;