/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const router = require('express').Router();
const adminAuth = require('../middleware/adminAuthenticate');

const projectController = require('../controllers/projectController');


// =============== Project ===================
router.post('/add/project', projectController.projectAddApi);

router.get('/list/project', projectController.projectListApi);

router.post('/project/edit/:id', projectController.projectEditApi);


module.exports = router;